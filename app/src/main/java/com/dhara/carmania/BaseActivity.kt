package com.dhara.carmania

import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.dhara.carmania.utils.FragmentController
import com.dhara.carmania.utils.getInstance
import android.content.Intent
import android.view.MenuItem


open class BaseActivity: AppCompatActivity() {
    private lateinit var fragmentController: FragmentController
    private var isStopped = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentController = getInstance(this)
    }

    override fun onStart() {
        super.onStart()
        isStopped = false
    }

    override fun onStop() {
        super.onStop()
        isStopped = true
    }

    fun <T : Fragment> switchFragment(fragClass: Class<T>, bundle: Bundle?, sharedViewList: List<View>) {
        fragmentController.onSwitchFragment(fragClass, bundle, sharedViewList)
    }

    @IdRes
    fun getContainerId(): Int {
        return R.id.frame_container
    }

    fun isStopped(): Boolean {
        return isStopped
    }

    override fun onBackPressed() {
        fragmentController.popFragmentFromStack()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}