package com.dhara.carmania.home.view

import android.view.View

interface ViewInteractionListener {
    fun onManufacturerClicked(view: View, key: Int)

    fun onLoadMore()
}