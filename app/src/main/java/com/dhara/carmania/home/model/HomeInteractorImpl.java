package com.dhara.carmania.home.model;

import com.dhara.carmania.CarManiaApp;
import com.dhara.carmania.common.ResponseListener;
import com.dhara.carmania.home.datasource.HomeDataSource;
import com.dhara.carmania.network.Listener;
import com.dhara.carmania.network.ServiceError;
import com.dhara.carmania.network.api.ApiKt;
import com.dhara.carmania.network.entity.ManufacturerResponse;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import static com.dhara.carmania.dagger2.injectors.CarManiaInjectorKt.getCarManiaComponent;

public class HomeInteractorImpl implements HomeInteractor {
    private final Map<Integer, String> manufacturers = new HashMap<>();
    private ManufacturerResponse manufacturerResponse;
    private int page;
    private long pageSize;

    @Inject
    HomeDataSource dataSource;

    @Inject
    public HomeInteractorImpl() {
        getCarManiaComponent(CarManiaApp.Companion.getInstance()).inject(this);
        page = 0;
        pageSize = 15;
    }

    @Override
    public void fetchManufacturers(final ResponseListener listener, final boolean isFirstTime) {
        if (isFirstTime) {
            page = 0;
        }

        dataSource.fetchManufacturers(ApiKt.getKey(), page, pageSize, new Listener<ManufacturerResponse>() {
            @Override
            public void onSuccess(final ManufacturerResponse response) {
                manufacturerResponse = response;

                if (isFirstTime) {
                    manufacturers.clear();
                }

                manufacturers.putAll(response.getCompany());

                if (isLoadMore()) {
                    page += 1;
                }

                listener.onSuccess();
            }

            @Override
            public void onError(@NotNull final ServiceError serviceError) {
                listener.onError(serviceError);
            }

            @Override
            public void onComplete() {
                // do nothing
            }
        });
    }

    @Override
    public Map<Integer, String> getManufacturers() {
        return manufacturers;
    }

    @Override
    public String getManufacturer(final int key) {
        return manufacturers.get(key);
    }

    @Override
    public boolean isLoadMore() {
        return manufacturerResponse != null && manufacturerResponse.getTotalPageCount() > page;
    }
}
