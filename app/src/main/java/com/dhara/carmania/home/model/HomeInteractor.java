package com.dhara.carmania.home.model;

import com.dhara.carmania.common.ResponseListener;

import java.util.Map;

public interface HomeInteractor {
    void fetchManufacturers(ResponseListener listener, boolean isFirstTime);

    Map<Integer, String> getManufacturers();

    String getManufacturer(int key);

    boolean isLoadMore();
}
