package com.dhara.carmania.home.view

import android.os.Bundle
import com.dhara.carmania.BaseActivity
import com.dhara.carmania.R

class HomeActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        switchFragment(HomeFragment::class.java, null, emptyList())
    }
}