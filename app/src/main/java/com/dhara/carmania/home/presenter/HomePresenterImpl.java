package com.dhara.carmania.home.presenter;

import android.view.View;

import com.dhara.carmania.CarManiaApp;
import com.dhara.carmania.common.ResponseListener;
import com.dhara.carmania.home.model.HomeInteractor;
import com.dhara.carmania.home.model.HomeModelAdapterImpl;
import com.dhara.carmania.home.router.HomeRouter;
import com.dhara.carmania.home.view.HomeView;
import com.dhara.carmania.home.view.ViewInteractionListener;
import com.dhara.carmania.network.ServiceError;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.dhara.carmania.dagger2.injectors.CarManiaInjectorKt.getCarManiaComponent;

public class HomePresenterImpl implements HomePresenter, ResponseListener, ViewInteractionListener {
    private HomeView view;
    private HomeRouter router;

    @Inject
    HomeInteractor interactor;

    public HomePresenterImpl() {
        getCarManiaComponent(CarManiaApp.Companion.getInstance()).inject(this);
    }

    @Override
    public void handleOnCreate(final HomeView view, final HomeRouter router) {
        this.view = view;
        this.router = router;

        view.initViews(new HomeModelAdapterImpl(interactor.getManufacturers()), this);
        view.showProgress();
        interactor.fetchManufacturers(this, true);
    }

    @Override
    public void onSuccess() {
        view.hideProgress();
        view.updateViews(new HomeModelAdapterImpl(interactor.getManufacturers()), this);
    }

    @Override
    public void onLoadMore() {
        if (interactor.isLoadMore()) {
            interactor.fetchManufacturers(this, false);
            return;
        }
        view.endLoadMore();
    }

    @Override
    public void onError(@NotNull final ServiceError error) {
        view.hideProgress();
        view.showMessage(error.getErrorMessage());
    }

    @Override
    public void onManufacturerClicked(final View view, final int key) {
        final List<View> viewList = new ArrayList<>();
        viewList.add(view);

        router.gotoPage(viewList, key);
    }
}
