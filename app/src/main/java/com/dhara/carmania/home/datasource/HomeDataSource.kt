package com.dhara.carmania.home.datasource

import com.dhara.carmania.network.Listener
import com.dhara.carmania.network.entity.ManufacturerResponse

interface HomeDataSource {
    fun fetchManufacturers(key: String,
                           page: Int,
                           pageSize: Long,
                           listener: Listener<ManufacturerResponse>)
}