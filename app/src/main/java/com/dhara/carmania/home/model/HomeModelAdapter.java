package com.dhara.carmania.home.model;

public interface HomeModelAdapter {
    int getCount();

    String getManufacturer(int position);

    Integer getKey(int position);

    int getLayout(int viewType);

    int getItemViewType(int position);

    boolean isViewEven(int position);
}
