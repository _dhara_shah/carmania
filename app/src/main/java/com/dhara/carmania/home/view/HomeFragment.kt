package com.dhara.carmania.home.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dhara.carmania.BaseFragment
import com.dhara.carmania.R
import com.dhara.carmania.home.presenter.HomePresenter
import com.dhara.carmania.home.router.HomeRouter
import javax.inject.Inject

class HomeFragment: BaseFragment() {
    @Inject
    lateinit var presenter: HomePresenter

    @Inject
    lateinit var router: HomeRouter

    @Inject
    lateinit var homeView: HomeView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        getFragmentComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.handleOnCreate(homeView, router)
    }
}