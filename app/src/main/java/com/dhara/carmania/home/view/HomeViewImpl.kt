package com.dhara.carmania.home.view

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.dhara.carmania.home.model.HomeModelAdapter
import com.dhara.carmania.utils.extensions.setVisible
import com.dhara.carmania.utils.extensions.showSnackBar
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

private var canLoadMore = true
private const val visibleThreshold = 5
private var totalItemCount: Int = 0
private var lastVisibleItem: Int = 0
private var isLoading: Boolean = false

class HomeViewImpl @Inject constructor(private val activity: AppCompatActivity) : HomeView {
    override fun initViews(modelAdapter: HomeModelAdapter, listener: ViewInteractionListener) {
        initToolbar()

        val adapter = HomeAdapter(modelAdapter, listener)
        val layoutManager = LinearLayoutManager(activity.applicationContext).apply {
            orientation = LinearLayoutManager.VERTICAL
        }

        with(activity.rvItems) {
            this.layoutManager = layoutManager
            setHasFixedSize(true)
        }
        activity.rvItems.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (canLoadMore) {
                    totalItemCount = layoutManager.itemCount
                    lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        listener.onLoadMore()
                        isLoading = true
                    }
                }
            }
        })
        activity.rvItems.adapter = adapter
    }

    override fun updateViews(modelAdapter: HomeModelAdapter, listener: ViewInteractionListener) {
        isLoading = false
        val adapter = HomeAdapter(modelAdapter, listener)
        activity.rvItems.swapAdapter(adapter, true)
    }

    override fun showProgress() {
        activity.progressBar.setVisible(true)
    }

    override fun hideProgress() {
        activity.progressBar.setVisible(false)
    }

    override fun showMessage(message: String?) {
        activity.rvItems.showSnackBar(message)
    }

    override fun endLoadMore() {
        canLoadMore = false
    }

    private fun initToolbar() {
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }
}