package com.dhara.carmania.home.view

import com.dhara.carmania.common.view.BaseView
import com.dhara.carmania.home.model.HomeModelAdapter

interface HomeView : BaseView {
    fun initViews(modelAdapter: HomeModelAdapter, listener: ViewInteractionListener)

    fun updateViews(modelAdapter: HomeModelAdapter, listener: ViewInteractionListener)

    fun endLoadMore()
}