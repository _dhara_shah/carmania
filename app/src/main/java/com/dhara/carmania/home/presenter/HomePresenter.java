package com.dhara.carmania.home.presenter;

import com.dhara.carmania.home.router.HomeRouter;
import com.dhara.carmania.home.view.HomeView;

public interface HomePresenter {
    void handleOnCreate(HomeView view, HomeRouter router);
}
