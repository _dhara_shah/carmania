package com.dhara.carmania.home.datasource

import com.dhara.carmania.CarManiaApp
import com.dhara.carmania.dagger2.injectors.getNetComponent
import com.dhara.carmania.network.Listener
import com.dhara.carmania.network.ServiceError
import com.dhara.carmania.network.api.RestApi
import com.dhara.carmania.network.entity.ManufacturerResponse
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class HomeDataSourceImpl @Inject constructor() : HomeDataSource {
    @Inject
    lateinit var restApi: RestApi

    init {
        getNetComponent(CarManiaApp.instance).inject(this)
    }

    override fun fetchManufacturers(key: String,
                                    page: Int,
                                    pageSize: Long,
                                    listener: Listener<ManufacturerResponse>) {
        restApi.getManufacturers(key, page, pageSize)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object: SingleObserver<ManufacturerResponse> {
                    override fun onSuccess(response: ManufacturerResponse) {
                        listener.onSuccess(response)
                    }

                    override fun onError(e: Throwable) {
                        listener.onError(ServiceError(e.message))
                    }

                    override fun onSubscribe(d: Disposable) {
                    }
                })
    }
}
