package com.dhara.carmania.home.router;

import android.os.Bundle;
import android.view.View;

import com.dhara.carmania.BaseActivity;
import com.dhara.carmania.type.view.TypeFragment;
import com.dhara.carmania.utils.ConstIntentExtrasKt;

import java.util.List;

import javax.inject.Inject;

public class HomeRouterImpl implements HomeRouter {
    private final BaseActivity activity;

    @Inject
    public HomeRouterImpl(final BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public void gotoPage(final List<View> viewList, final long value) {
        final Bundle bundle = new Bundle();
        bundle.putLong(ConstIntentExtrasKt.EXTRA_MANUFACTURER, value);
        activity.switchFragment(TypeFragment.class, bundle, viewList);
    }
}
