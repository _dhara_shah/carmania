package com.dhara.carmania.home.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dhara.carmania.common.entity.RowType
import com.dhara.carmania.home.model.HomeModelAdapter
import kotlinx.android.synthetic.main.list_item_even.view.*
import kotlinx.android.synthetic.main.list_item_odd.view.*

class HomeAdapter(private val modelAdapter: HomeModelAdapter,
                  private val listener: ViewInteractionListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutRes = modelAdapter.getLayout(viewType)
        val itemView = LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
        return if (viewType == RowType.EVEN_ROW.rowType)
            ManufacturerEvenVH(itemView, listener, modelAdapter)
        else
            ManufacturerOddVH(itemView, listener, modelAdapter)
    }

    override fun getItemCount(): Int {
        return modelAdapter.count
    }

    override fun getItemViewType(position: Int): Int {
        return modelAdapter.getItemViewType(position)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (modelAdapter.isViewEven(position)) {
            (holder as ManufacturerEvenVH).txtManufacturer?.text = modelAdapter.getManufacturer(position)
        } else {
            (holder as ManufacturerOddVH).txtManufacturer?.text = modelAdapter.getManufacturer(position)
        }
    }

    class ManufacturerEvenVH(itemView: View,
                         listener: ViewInteractionListener,
                         modelAdapter: HomeModelAdapter) : RecyclerView.ViewHolder(itemView) {
        val txtManufacturer: TextView? = itemView.txtListItem

        init {
            txtManufacturer?.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    listener.onManufacturerClicked(itemView, modelAdapter.getKey(adapterPosition))
                }
            }
        }
    }

    class ManufacturerOddVH(itemView: View,
                            listener: ViewInteractionListener,
                            modelAdapter: HomeModelAdapter): RecyclerView.ViewHolder(itemView) {
        val txtManufacturer: TextView? = itemView.txtListItemOdd

        init {
            txtManufacturer?.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    listener.onManufacturerClicked(itemView, modelAdapter.getKey(adapterPosition))
                }
            }
        }
    }
}