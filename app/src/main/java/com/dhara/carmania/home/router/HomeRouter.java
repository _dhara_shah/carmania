package com.dhara.carmania.home.router;

import android.view.View;

import java.util.List;

public interface HomeRouter {
    void gotoPage(List<View> viewList, long value);
}
