package com.dhara.carmania.common.view

interface BaseView {
    fun showProgress()

    fun hideProgress()

    fun showMessage(message : String?)
}