package com.dhara.carmania.common.entity

enum class RowType(val rowType: Int) {
    EVEN_ROW(0),
    ODD_ROW(1)
}