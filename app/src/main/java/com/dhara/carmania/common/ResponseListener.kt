package com.dhara.carmania.common

import com.dhara.carmania.network.ServiceError

interface ResponseListener {
    fun onSuccess()

    fun onError(error : ServiceError)
}