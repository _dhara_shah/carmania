package com.dhara.carmania.network.entity

data class TypeResponse(
        val page: Long,
        val pageSize: Long, // number of records per page
        val totalPageCount: Int, // number of pages to go for complete result
        val company: Map<String, String>
)