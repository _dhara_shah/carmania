package com.dhara.carmania.network.api

private const val BASE_URL: String = "https://api-aws-eu-qa-1.auto1-test.com/"
private const val KEY_VALUE: String = "coding-puzzle-client-449cc9d"

fun getHost(): String {
    return BASE_URL
}

fun getKey(): String {
    return KEY_VALUE
}