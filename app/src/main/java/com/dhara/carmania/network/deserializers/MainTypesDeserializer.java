package com.dhara.carmania.network.deserializers;

import com.dhara.carmania.network.entity.TypeResponse;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class MainTypesDeserializer implements JsonDeserializer<TypeResponse> {
    @Override
    public TypeResponse deserialize(final JsonElement json,
                                            final Type typeOfT,
                                            final JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        final JsonObject companyObj = jsonObject.get("wkda").getAsJsonObject();
        final Map<String, String> mainTypes = new HashMap<>();

        final Long page = jsonObject.get("page").getAsLong();
        final Long pageSize = jsonObject.get("pageSize").getAsLong();
        final Integer totalPageCount = jsonObject.get("totalPageCount").getAsInt();

        for (final Map.Entry<String, JsonElement> entry : companyObj.entrySet()) {
            mainTypes.put(entry.getKey(), entry.getValue().getAsString());
        }
        return new TypeResponse(page, pageSize, totalPageCount, mainTypes);
    }
}
