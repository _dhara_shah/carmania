package com.dhara.carmania.network.entity

data class BuildsResponse(
        val company: Map<Int, String>
)