package com.dhara.carmania.network.deserializers;

import com.dhara.carmania.network.entity.BuildsResponse;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class BuildsDeserializer implements JsonDeserializer<BuildsResponse> {
    @Override
    public BuildsResponse deserialize(final JsonElement json,
                                            final Type typeOfT,
                                            final JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        final JsonObject companyObj = jsonObject.get("wkda").getAsJsonObject();
        final Map<Integer, String> buildDates = new HashMap<>();

        for (final Map.Entry<String, JsonElement> entry : companyObj.entrySet()) {
            buildDates.put(Integer.parseInt(entry.getKey()), entry.getValue().getAsString());
        }
        return new BuildsResponse(buildDates);
    }
}
