package com.dhara.carmania.network.api

import com.dhara.carmania.network.entity.BuildsResponse
import com.dhara.carmania.network.entity.ManufacturerResponse
import com.dhara.carmania.network.entity.TypeResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RestApi {
    @GET("$VERSION_1/car-types/manufacturer")
    fun getManufacturers(@Query(KEY) key: String,
                         @Query("page") page: Int,
                         @Query("pageSize") pageSize: Long): Single<ManufacturerResponse>

    @GET("$VERSION_1/car-types/main-types")
    fun getTypes(@Query(KEY) key: String,
                 @Query("manufacturer") manufacturer: Long,
                 @Query("page") page: Int,
                 @Query("pageSize") pageSize: Long): Single<TypeResponse>

    @GET("$VERSION_1/car-types/built-dates")
    fun getBuildDates(@Query(KEY) key: String,
                      @Query("manufacturer") manufacturer: Long,
                      @Query("main-type") type: String): Single<BuildsResponse>
}