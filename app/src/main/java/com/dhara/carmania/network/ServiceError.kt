package com.dhara.carmania.network

data class ServiceError @JvmOverloads constructor(var errorMessage: String? = "",
                                                  var errorCode: String? = "")