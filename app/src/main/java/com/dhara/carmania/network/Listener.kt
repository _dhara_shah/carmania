package com.dhara.carmania.network

interface Listener<T> {
    fun onSuccess(response : T)

    fun onError(serviceError: ServiceError)

    fun onComplete()
}