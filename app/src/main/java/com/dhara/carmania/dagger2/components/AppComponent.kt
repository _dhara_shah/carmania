package com.dhara.carmania.dagger2.components

import android.app.Application
import com.dhara.carmania.dagger2.modules.AppModule
import com.dhara.carmania.CarManiaApp
import com.dhara.carmania.utils.CarManiaLog
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component (modules = [AppModule::class])
interface AppComponent {
    fun inject(app: CarManiaApp)

    fun inject(carManiaLog: CarManiaLog)

    fun application() :Application

    fun getCarManiaLog() : CarManiaLog
}