package com.dhara.carmania.dagger2.modules

import com.dhara.carmania.builds.datasource.BuildsDataSource
import com.dhara.carmania.builds.datasource.BuildsDataSourceImpl
import com.dhara.carmania.home.datasource.HomeDataSource
import com.dhara.carmania.home.datasource.HomeDataSourceImpl
import com.dhara.carmania.home.model.HomeInteractor
import com.dhara.carmania.home.model.HomeInteractorImpl
import com.dhara.carmania.type.datasource.TypeDataSource
import com.dhara.carmania.type.datasource.TypeDataSourceImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides

@Module
class CarManiaModule {
    @Provides
    fun provideGson(): Gson {
        return Gson()
    }

    // other interactors and date source here
    @Provides
    fun provideHomeInteractor(): HomeInteractor = HomeInteractorImpl()

    @Provides
    fun provideHomeDataSource(): HomeDataSource = HomeDataSourceImpl()

    @Provides
    fun provideTypeDataSource(): TypeDataSource = TypeDataSourceImpl()

    @Provides
    fun provideBuildsDataSource(): BuildsDataSource = BuildsDataSourceImpl()
}