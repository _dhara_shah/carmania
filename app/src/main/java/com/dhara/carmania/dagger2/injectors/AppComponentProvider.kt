package com.dhara.carmania.dagger2.injectors

import android.content.Context
import com.dhara.carmania.dagger2.components.AppComponent

interface AppComponentProvider {
    fun getAppComponent(context: Context): AppComponent
}