package com.dhara.carmania.dagger2.injectors

import android.content.Context
import com.dhara.carmania.dagger2.components.AppComponent

fun getAppComponent(context: Context): AppComponent {
    val provider: AppComponentProvider = context as AppComponentProvider
    return provider.getAppComponent(context)
}