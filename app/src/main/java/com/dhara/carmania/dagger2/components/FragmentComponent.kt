package com.dhara.carmania.dagger2.components

import com.dhara.carmania.builds.view.BuildFragment
import com.dhara.carmania.dagger2.injection.PerFragment
import com.dhara.carmania.dagger2.modules.FragmentModule
import com.dhara.carmania.home.view.HomeFragment
import com.dhara.carmania.type.view.TypeFragment
import dagger.Component

@PerFragment
@Component(modules = [FragmentModule::class], dependencies = [AppComponent::class, CarManiaComponent::class])
interface FragmentComponent {
    fun inject(homeFragment: HomeFragment)

    fun inject(homeFragment: TypeFragment)

    fun inject(buildFragment: BuildFragment)
}