package com.dhara.carmania.dagger2.components

import com.dhara.carmania.builds.model.BuildsInteractorImpl
import com.dhara.carmania.dagger2.modules.AppModule
import com.dhara.carmania.dagger2.modules.CarManiaModule
import com.dhara.carmania.home.model.HomeInteractorImpl
import com.dhara.carmania.home.presenter.HomePresenterImpl
import com.dhara.carmania.type.model.TypeInteractorImpl
import dagger.Component

@Component(modules = [CarManiaModule::class, AppModule::class])
interface CarManiaComponent {
    // inject presenters and interactors

    fun inject(homePresenter: HomePresenterImpl)

    fun inject(homeInteractor: HomeInteractorImpl)

    fun inject(typeInteractor: TypeInteractorImpl)

    fun inject(buildsInteractor: BuildsInteractorImpl)
}