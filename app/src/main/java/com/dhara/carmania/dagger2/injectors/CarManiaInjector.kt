package com.dhara.carmania.dagger2.injectors

import android.content.Context
import com.dhara.carmania.dagger2.components.CarManiaComponent

fun getCarManiaComponent(context: Context): CarManiaComponent {
    val provider: CarManiaComponentProvider = context as CarManiaComponentProvider
    return provider.getCarManiaComponent(context)
}