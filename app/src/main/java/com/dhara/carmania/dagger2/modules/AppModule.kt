package com.dhara.carmania.dagger2.modules

import android.app.Application
import com.dhara.carmania.utils.CarManiaLog
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Singleton
@Module
class AppModule (private val application : Application){

    @Provides
    fun provideApplication() : Application = application

    @Provides
    fun provideCarManiaLog() : CarManiaLog {
        return CarManiaLog()
    }
}