package com.dhara.carmania.dagger2.injectors

import android.content.Context
import com.dhara.carmania.dagger2.components.NetComponent

interface NetComponentProvider {
    fun getNetComponent(context: Context): NetComponent
}