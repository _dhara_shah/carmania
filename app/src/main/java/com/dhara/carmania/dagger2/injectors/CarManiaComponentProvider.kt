package com.dhara.carmania.dagger2.injectors

import android.content.Context
import com.dhara.carmania.dagger2.components.CarManiaComponent

interface CarManiaComponentProvider {
    fun getCarManiaComponent(context: Context): CarManiaComponent
}