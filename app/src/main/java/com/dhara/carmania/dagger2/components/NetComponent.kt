package com.dhara.carmania.dagger2.components

import com.dhara.carmania.builds.datasource.BuildsDataSourceImpl
import com.dhara.carmania.dagger2.modules.AppModule
import com.dhara.carmania.dagger2.modules.NetModule
import com.dhara.carmania.network.api.RestApi
import dagger.Component
import javax.inject.Singleton
import com.dhara.carmania.home.datasource.HomeDataSourceImpl
import com.dhara.carmania.type.datasource.TypeDataSourceImpl

@Singleton
@Component (modules = [AppModule:: class, NetModule::class])
interface NetComponent {
    // inject all data sources here
    fun getRestApi(): RestApi

    fun inject(homeDataSource: HomeDataSourceImpl)

    fun inject(typeDataSource: TypeDataSourceImpl)

    fun inject(buildsDataSource: BuildsDataSourceImpl)
}