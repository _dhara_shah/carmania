package com.dhara.carmania.dagger2.injectors

import android.content.Context
import com.dhara.carmania.dagger2.components.NetComponent

fun getNetComponent(context: Context): NetComponent {
    val provider: NetComponentProvider = context as NetComponentProvider
    return provider.getNetComponent(context)
}