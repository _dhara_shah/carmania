package com.dhara.carmania.dagger2.modules

import android.content.Context
import android.support.v7.app.AppCompatActivity
import com.dhara.carmania.BaseActivity
import com.dhara.carmania.builds.model.BuildsInteractor
import com.dhara.carmania.builds.model.BuildsInteractorImpl
import com.dhara.carmania.builds.presenter.BuildsPresenter
import com.dhara.carmania.builds.presenter.BuildsPresenterImpl
import com.dhara.carmania.builds.router.BuildsRouter
import com.dhara.carmania.builds.router.BuildsRouterImpl
import com.dhara.carmania.builds.view.BuildView
import com.dhara.carmania.builds.view.BuildViewImpl
import com.dhara.carmania.dagger2.injection.PerFragment
import com.dhara.carmania.dagger2.scopes.ActivityScope
import com.dhara.carmania.home.presenter.HomePresenter
import com.dhara.carmania.home.presenter.HomePresenterImpl
import com.dhara.carmania.home.router.HomeRouter
import com.dhara.carmania.home.router.HomeRouterImpl
import com.dhara.carmania.home.view.HomeView
import com.dhara.carmania.home.view.HomeViewImpl
import com.dhara.carmania.type.model.TypeInteractor
import com.dhara.carmania.type.model.TypeInteractorImpl
import com.dhara.carmania.type.presenter.TypePresenter
import com.dhara.carmania.type.presenter.TypePresenterImpl
import com.dhara.carmania.type.router.TypeRouter
import com.dhara.carmania.type.router.TypeRouterImpl
import com.dhara.carmania.type.view.TypeView
import com.dhara.carmania.type.view.TypeViewImpl
import dagger.Module
import dagger.Provides

@Module
class FragmentModule(private val activity: AppCompatActivity) {
    @Provides
    fun provideActivity(): AppCompatActivity = activity

    @Provides
    @ActivityScope
    fun provideContext(): Context = activity

    @Provides
    @PerFragment
    fun provideHomePresenter(): HomePresenter = HomePresenterImpl()

    @Provides
    @PerFragment
    fun provideHomeView(): HomeView = HomeViewImpl(activity)

    @Provides
    @PerFragment
    fun provideHomeRouter(): HomeRouter = HomeRouterImpl(activity as BaseActivity)

    @Provides
    @PerFragment
    fun provideTypePresenter(): TypePresenter = TypePresenterImpl()

    @Provides
    @PerFragment
    fun provideTypeInteractor(): TypeInteractor = TypeInteractorImpl()

    @Provides
    @PerFragment
    fun provideTypeView(): TypeView = TypeViewImpl(activity)

    @Provides
    @PerFragment
    fun provideTypeRouter(): TypeRouter = TypeRouterImpl(activity as BaseActivity)

    @Provides
    @PerFragment
    fun provideBuildsPresenter(): BuildsPresenter = BuildsPresenterImpl()

    @Provides
    @PerFragment
    fun provideBuildsInteractor(): BuildsInteractor = BuildsInteractorImpl()

    @Provides
    @PerFragment
    fun provideBuildsView(): BuildView = BuildViewImpl(activity)

    @Provides
    @PerFragment
    fun provideBuildsRouter(): BuildsRouter = BuildsRouterImpl(activity as BaseActivity)
}