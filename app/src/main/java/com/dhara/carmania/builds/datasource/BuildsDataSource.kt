package com.dhara.carmania.builds.datasource

import com.dhara.carmania.network.Listener
import com.dhara.carmania.network.entity.BuildsResponse

interface BuildsDataSource {
    fun fetchBuildDates(key: String,
                        manufacturer: Long,
                        type: String,
                        listener: Listener<BuildsResponse>)
}