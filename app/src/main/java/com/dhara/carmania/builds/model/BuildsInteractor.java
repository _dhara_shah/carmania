package com.dhara.carmania.builds.model;

import android.os.Bundle;

import com.dhara.carmania.common.ResponseListener;

import java.util.Map;

public interface BuildsInteractor {
    void setArguments(Bundle args);

    void fetchBuildDates(ResponseListener listener);

    Map<Integer, String> getBuildDates();
}
