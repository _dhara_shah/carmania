package com.dhara.carmania.builds.model;

import android.os.Bundle;

import com.dhara.carmania.CarManiaApp;
import com.dhara.carmania.builds.datasource.BuildsDataSource;
import com.dhara.carmania.common.ResponseListener;
import com.dhara.carmania.network.Listener;
import com.dhara.carmania.network.ServiceError;
import com.dhara.carmania.network.api.ApiKt;
import com.dhara.carmania.network.entity.BuildsResponse;
import com.dhara.carmania.utils.ConstIntentExtrasKt;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import javax.inject.Inject;

import static com.dhara.carmania.dagger2.injectors.CarManiaInjectorKt.getCarManiaComponent;

public class BuildsInteractorImpl implements BuildsInteractor {
    private Map<Integer, String> buildDates;
    private Bundle args;

    @Inject
    BuildsDataSource dataSource;

    public BuildsInteractorImpl() {
        getCarManiaComponent(CarManiaApp.Companion.getInstance()).inject(this);
    }

    @Override
    public void setArguments(final Bundle args) {
        this.args = args;
    }

    @Override
    public void fetchBuildDates(final ResponseListener listener) {
        dataSource.fetchBuildDates(ApiKt.getKey(), getManufacturer(), getType(), new Listener<BuildsResponse>() {
            @Override
            public void onSuccess(final BuildsResponse response) {
                buildDates = response.getCompany();
                listener.onSuccess();
            }

            @Override
            public void onError(@NotNull final ServiceError serviceError) {
                listener.onError(serviceError);
            }

            @Override
            public void onComplete() {
                // do nothing
            }
        });
    }

    @Override
    public Map<Integer, String> getBuildDates() {
        return buildDates;
    }

    private String getType() {
        return args != null ? args.getString(ConstIntentExtrasKt.EXTRA_MAIN_TYPE) : "";
    }

    private long getManufacturer() {
        return args != null ? args.getLong(ConstIntentExtrasKt.EXTRA_MANUFACTURER) : -1;
    }
}
