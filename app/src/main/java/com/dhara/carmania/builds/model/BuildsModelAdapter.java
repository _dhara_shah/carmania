package com.dhara.carmania.builds.model;

public interface BuildsModelAdapter {
    int getCount();

    String getBuildDate(int position);

    Integer getKey(int position);

    int getLayout(int viewType);

    int getItemViewType(int position);

    boolean isViewEven(int position);

}
