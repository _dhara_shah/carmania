package com.dhara.carmania.builds.presenter;

import com.dhara.carmania.builds.model.BuildsInteractor;
import com.dhara.carmania.builds.model.BuildsModelAdapterImpl;
import com.dhara.carmania.builds.router.BuildsRouter;
import com.dhara.carmania.builds.view.BuildView;
import com.dhara.carmania.common.ResponseListener;
import com.dhara.carmania.network.ServiceError;

import org.jetbrains.annotations.NotNull;

public class BuildsPresenterImpl implements BuildsPresenter, ResponseListener, BuildView.ViewInteractionListener {
    private BuildView view;
    private BuildsRouter router;
    private BuildsInteractor interactor;

    @Override
    public void handleOnCreate(final BuildView view, final BuildsInteractor interactor, final BuildsRouter router) {
        this.view = view;
        this.interactor = interactor;
        this.router = router;

        view.initViews(new BuildsModelAdapterImpl(interactor.getBuildDates()), this);
        view.showProgress();
        interactor.fetchBuildDates(this);
    }

    @Override
    public void onSuccess() {
        view.hideProgress();
        view.updateViews(new BuildsModelAdapterImpl(interactor.getBuildDates()), this);
    }

    @Override
    public void onError(@NotNull final ServiceError error) {
        view.hideProgress();
        view.showMessage(error.getErrorMessage());
    }

    @Override
    public void onBuildClicked(final int key) {
        //router.gotoPage(interactor.getManufacturer(key));
    }
}
