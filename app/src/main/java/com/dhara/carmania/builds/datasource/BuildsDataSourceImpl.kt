package com.dhara.carmania.builds.datasource

import com.dhara.carmania.CarManiaApp
import com.dhara.carmania.dagger2.injectors.getNetComponent
import com.dhara.carmania.network.Listener
import com.dhara.carmania.network.ServiceError
import com.dhara.carmania.network.api.RestApi
import com.dhara.carmania.network.entity.BuildsResponse
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class BuildsDataSourceImpl @Inject constructor() : BuildsDataSource {
    @Inject
    lateinit var restApi: RestApi

    init {
        getNetComponent(CarManiaApp.instance).inject(this)
    }

    override fun fetchBuildDates(key: String,
                                 manufacturer: Long,
                                 type: String,
                                 listener: Listener<BuildsResponse>) {
        restApi.getBuildDates(key, manufacturer, type)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<BuildsResponse> {
                    override fun onSuccess(response: BuildsResponse) {
                        listener.onSuccess(response)
                    }

                    override fun onError(e: Throwable) {
                        listener.onError(ServiceError(e.message))
                    }

                    override fun onSubscribe(d: Disposable) {
                    }
                })
    }
}