package com.dhara.carmania.builds.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dhara.carmania.BaseFragment
import com.dhara.carmania.R
import com.dhara.carmania.builds.model.BuildsInteractor
import com.dhara.carmania.builds.presenter.BuildsPresenter
import com.dhara.carmania.builds.router.BuildsRouter
import javax.inject.Inject

class BuildFragment: BaseFragment() {
    @Inject
    lateinit var presenter: BuildsPresenter

    @Inject
    lateinit var router: BuildsRouter

    @Inject
    lateinit var buildView: BuildView

    @Inject
    lateinit var interactor: BuildsInteractor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        getFragmentComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        interactor.setArguments(arguments)
        presenter.handleOnCreate(buildView, interactor, router)
    }
}