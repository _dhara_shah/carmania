package com.dhara.carmania.builds.router;

import com.dhara.carmania.BaseActivity;

import javax.inject.Inject;

public class BuildsRouterImpl implements BuildsRouter {
    final BaseActivity activity;

    @Inject
    public BuildsRouterImpl(final BaseActivity activity) {
        this.activity = activity;
    }
}
