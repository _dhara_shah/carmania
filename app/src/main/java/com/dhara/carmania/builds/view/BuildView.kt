package com.dhara.carmania.builds.view

import com.dhara.carmania.builds.model.BuildsModelAdapter
import com.dhara.carmania.common.view.BaseView

interface BuildView: BaseView {
    fun initViews(modelAdapter: BuildsModelAdapter, listener: ViewInteractionListener)

    fun updateViews(modelAdapter: BuildsModelAdapter, listener: ViewInteractionListener)

    interface ViewInteractionListener {
        fun onBuildClicked(key: Int)
    }
}