package com.dhara.carmania.builds.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dhara.carmania.builds.model.BuildsModelAdapter
import com.dhara.carmania.common.entity.RowType
import kotlinx.android.synthetic.main.list_item_even.view.*
import kotlinx.android.synthetic.main.list_item_odd.view.*

class BuildAdapter(private val modelAdapter: BuildsModelAdapter,
                   private val listener: BuildView.ViewInteractionListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutRes = modelAdapter.getLayout(viewType)
        val itemView = LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
        return if (viewType == RowType.EVEN_ROW.rowType) BuildEvenVH(itemView, modelAdapter, listener)
        else BuildOddVH(itemView, modelAdapter, listener)
    }

    override fun getItemCount(): Int {
        return modelAdapter.count
    }

    override fun getItemViewType(position: Int): Int {
        return modelAdapter.getItemViewType(position)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (modelAdapter.isViewEven(position)) {
            (holder as BuildEvenVH).txtType?.text = modelAdapter.getBuildDate(position)
        } else {
            (holder as BuildOddVH).txtType?.text = modelAdapter.getBuildDate(position)
        }
    }

    class BuildEvenVH(itemView: View,
                      modelAdapter: BuildsModelAdapter,
                      listener: BuildView.ViewInteractionListener): RecyclerView.ViewHolder(itemView) {
        val txtType: TextView? = itemView.txtListItem

        init {
            txtType?.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    listener.onBuildClicked(modelAdapter.getKey(adapterPosition))
                }
            }
        }
    }

    class BuildOddVH(itemView: View,
                     modelAdapter: BuildsModelAdapter,
                     listener: BuildView.ViewInteractionListener): RecyclerView.ViewHolder(itemView) {
        val txtType: TextView? = itemView.txtListItemOdd

        init {
            txtType?.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    listener.onBuildClicked(modelAdapter.getKey(adapterPosition))
                }
            }
        }
    }
}