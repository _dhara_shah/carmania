package com.dhara.carmania.builds.presenter;

import com.dhara.carmania.builds.model.BuildsInteractor;
import com.dhara.carmania.builds.router.BuildsRouter;
import com.dhara.carmania.builds.view.BuildView;

public interface BuildsPresenter {
    void handleOnCreate(BuildView view, BuildsInteractor interactor, BuildsRouter router);
}
