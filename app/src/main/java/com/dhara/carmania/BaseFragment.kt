package com.dhara.carmania

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.dhara.carmania.dagger2.components.DaggerFragmentComponent
import com.dhara.carmania.dagger2.components.FragmentComponent
import com.dhara.carmania.dagger2.modules.FragmentModule

open class BaseFragment: Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun getFragmentComponent() : FragmentComponent {
        return DaggerFragmentComponent.builder()
                .apply {
                    fragmentModule(FragmentModule(this@BaseFragment.requireActivity() as AppCompatActivity))
                    appComponent(CarManiaApp.instance.appComponent)
                    carManiaComponent(CarManiaApp.instance.carManiaComponent)
                }
                .build()
    }
}