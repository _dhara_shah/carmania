package com.dhara.carmania

import android.app.Application
import android.content.Context
import com.dhara.carmania.dagger2.components.*
import com.dhara.carmania.dagger2.injectors.AppComponentProvider
import com.dhara.carmania.dagger2.injectors.CarManiaComponentProvider
import com.dhara.carmania.dagger2.injectors.NetComponentProvider
import com.dhara.carmania.dagger2.modules.AppModule
import com.dhara.carmania.dagger2.modules.CarManiaModule
import com.dhara.carmania.dagger2.modules.NetModule
import com.dhara.carmania.network.api.getHost

open class CarManiaApp : Application(), CarManiaComponentProvider, NetComponentProvider, AppComponentProvider {
    companion object {
        lateinit var instance: CarManiaApp
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    val appComponent : AppComponent by lazy {
        DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    val carManiaComponent : CarManiaComponent by lazy {
        DaggerCarManiaComponent.builder()
                .appModule(AppModule(this))
                .carManiaModule(CarManiaModule())
                .build()
    }

    private val netComponent : NetComponent by lazy {
        DaggerNetComponent.builder()
                .appModule(AppModule(this))
                .netModule(NetModule(getHost()))
                .build()
    }


    override fun getNetComponent(context: Context): NetComponent {
        return netComponent
    }

    override fun getCarManiaComponent(context: Context): CarManiaComponent {
        return carManiaComponent
    }

    override fun getAppComponent(context: Context): AppComponent {
        return appComponent
    }
}