package com.dhara.carmania.utils

fun <T> isEmpty(collection: Collection<T>?): Boolean {
    return collection == null || collection.isEmpty()
}

fun <K, V> isEmpty(map: Map<K, V>?): Boolean {
    return map == null || map.isEmpty()
}

fun <K, V> getValue(map: Map<K, V>, position: Int): V {
    return map.entries.elementAt(position).value
}

fun <K, V> getKey(map: Map<K, V>, position: Int): K {
    return map.entries.elementAt(position).key
}