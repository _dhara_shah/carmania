package com.dhara.carmania.utils

import android.util.Log
import com.dhara.carmania.BuildConfig
import com.dhara.carmania.CarManiaApp

class CarManiaLog {
    init {
        (CarManiaApp.instance).appComponent.inject(this)
    }

    fun d(tag : String, msg: String?) {
        if (useLog()) {
            msg?.let {
                Log.d(tag, msg)
            }
        }
    }

    fun e(tag: String, msg: String?) {
        if (useLog()) {
            msg?.let {
                Log.e(tag, msg)
            }
        }

    }

    fun writeToFile(tag: String, throwable: Throwable) {
        Log.wtf(tag, throwable)
    }

    private fun useLog() : Boolean {
        return BuildConfig.DEBUG
    }
}