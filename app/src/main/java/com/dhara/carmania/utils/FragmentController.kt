package com.dhara.carmania.utils

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.transition.*
import android.view.View
import com.dhara.carmania.BaseActivity
import com.dhara.carmania.R
import com.dhara.carmania.home.view.HomeFragment

const val FRAGMENT_TAG = "fragment_tag"

fun getInstance(baseActivity: BaseActivity): FragmentController {
    return FragmentController(baseActivity)
}

class FragmentController(private val activity: BaseActivity?) {

    fun <T : Fragment> onSwitchFragment(clazz: Class<T>, bundle: Bundle?, sharedViewList: List<View>) {
        if (activity == null || activity.isFinishing) {
            return
        }
        val fragment = createFragment(clazz)
        fragment.arguments = bundle

        val transaction = activity.supportFragmentManager.beginTransaction()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && sharedViewList.isNotEmpty()) run {
            val shownFragment = getActiveFragment()

            shownFragment?.let {
                val transitionSet = createTransitionSet()

                shownFragment.sharedElementReturnTransition = transitionSet
                shownFragment.exitTransition = Fade()
                fragment.sharedElementEnterTransition = transitionSet
                fragment.enterTransition = Fade()
            }
        }

        with(transaction) {
            replace(activity.getContainerId(), fragment, FRAGMENT_TAG)
            for (view in sharedViewList) {
                addSharedElement(view,
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) view.transitionName
                        else activity.getString(R.string.transition_name)
                )
            }
            addToBackStack(if (clazz == HomeFragment::class) null else clazz.toString())
            commit()
        }
    }

    private fun <T : Fragment> createFragment(clazz: Class<T>): T {
        try {
            val constructor = clazz.getConstructor()
            return constructor.newInstance()
        } catch (e: Exception) {
            throw IllegalArgumentException(String.format("%s is not appropriate class", clazz.toString()), e)
        }
    }

    private fun getActiveFragment(): Fragment? {
        return activity?.supportFragmentManager?.findFragmentByTag(FRAGMENT_TAG)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun createTransitionSet(): TransitionSet {
        val transitionSet = TransitionSet()

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            transitionSet.addTransition(ChangeClipBounds())
        } else {
            transitionSet.addTransition(ChangeBounds())
        }

        transitionSet.addTransition(Fade())
        transitionSet.addTransition(ChangeTransform())
        return transitionSet
    }

    fun popFragmentFromStack(): Boolean {
        if (activity == null || activity.isFinishing || activity.isStopped()) {
            return false
        }

        if (activity.supportFragmentManager.backStackEntryCount > 1) {
            activity.supportFragmentManager.popBackStackImmediate()
            return true
        }

        activity.finish()
        return false
    }
}