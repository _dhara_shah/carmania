package com.dhara.carmania.utils.extensions

import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.view.View

fun View.setVisible(visible : Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.showSnackBar(message: String?) {
    message?.let {
        Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
    }
}

fun View.showSnackBar(@StringRes messageRes: Int) {
    this.showSnackBar(this.context.getString(messageRes))
}