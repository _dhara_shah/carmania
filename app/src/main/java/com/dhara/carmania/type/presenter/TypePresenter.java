package com.dhara.carmania.type.presenter;

import com.dhara.carmania.type.model.TypeInteractor;
import com.dhara.carmania.type.router.TypeRouter;
import com.dhara.carmania.type.view.TypeView;

public interface TypePresenter {
    void handleOnCreate(TypeView view, TypeInteractor interactor, TypeRouter router);
}
