package com.dhara.carmania.type.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dhara.carmania.BaseFragment
import com.dhara.carmania.R
import com.dhara.carmania.type.model.TypeInteractor
import com.dhara.carmania.type.presenter.TypePresenter
import com.dhara.carmania.type.router.TypeRouter
import javax.inject.Inject

class TypeFragment: BaseFragment() {
    @Inject
    lateinit var presenter: TypePresenter

    @Inject
    lateinit var router: TypeRouter

    @Inject
    lateinit var typeView: TypeView

    @Inject
    lateinit var interactor: TypeInteractor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        getFragmentComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        interactor.setArguments(arguments)
        presenter.handleOnCreate(typeView, interactor, router)
    }
}