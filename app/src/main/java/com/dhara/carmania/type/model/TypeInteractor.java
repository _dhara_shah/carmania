package com.dhara.carmania.type.model;

import android.os.Bundle;

import com.dhara.carmania.common.ResponseListener;

import java.util.Map;

public interface TypeInteractor {
    void setArguments(Bundle args);

    void fetchTypes(ResponseListener listener);

    Map<String, String> getTypes();

    String getType(String key);

    long getManufacturer();
}
