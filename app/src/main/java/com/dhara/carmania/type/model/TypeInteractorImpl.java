package com.dhara.carmania.type.model;

import android.os.Bundle;

import com.dhara.carmania.CarManiaApp;
import com.dhara.carmania.common.ResponseListener;
import com.dhara.carmania.network.Listener;
import com.dhara.carmania.network.ServiceError;
import com.dhara.carmania.network.api.ApiKt;
import com.dhara.carmania.network.entity.TypeResponse;
import com.dhara.carmania.type.datasource.TypeDataSource;
import com.dhara.carmania.utils.ConstIntentExtrasKt;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import javax.inject.Inject;

import static com.dhara.carmania.dagger2.injectors.CarManiaInjectorKt.getCarManiaComponent;

public class TypeInteractorImpl implements TypeInteractor {
    private Map<String, String> types;
    private int page;
    private long pageSize;
    private Bundle args;

    @Inject
    TypeDataSource dataSource;

    public TypeInteractorImpl() {
        getCarManiaComponent(CarManiaApp.Companion.getInstance()).inject(this);
        page = 0;
        pageSize = 15;
    }

    @Override
    public void setArguments(final Bundle args) {
        this.args = args;
    }

    @Override
    public void fetchTypes(final ResponseListener listener) {
        dataSource.fetchTypes(ApiKt.getKey(), getManufacturer(), page, pageSize, new Listener<TypeResponse>() {
            @Override
            public void onSuccess(final TypeResponse response) {
                types = response.getCompany();
                listener.onSuccess();
            }

            @Override
            public void onError(@NotNull final ServiceError serviceError) {
                listener.onError(serviceError);
            }

            @Override
            public void onComplete() {
                // do nothing
            }
        });
    }

    @Override
    public Map<String, String> getTypes() {
        return types;
    }

    @Override
    public String getType(final String key) {
        return types.get(key);
    }

    @Override
    public long getManufacturer() {
        return args != null ? args.getLong(ConstIntentExtrasKt.EXTRA_MANUFACTURER) : -1;
    }
}
