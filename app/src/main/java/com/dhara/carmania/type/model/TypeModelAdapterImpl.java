package com.dhara.carmania.type.model;

import com.dhara.carmania.R;
import com.dhara.carmania.common.entity.RowType;
import com.dhara.carmania.utils.CollectionUtilsKt;

import java.util.Map;

public class TypeModelAdapterImpl implements TypeModelAdapter {
    private final Map<String, String> map;

    public TypeModelAdapterImpl(final Map<String, String> types) {
        this.map = types;
    }

    @Override
    public int getCount() {
        return CollectionUtilsKt.isEmpty(map) ? 0 :  map.entrySet().size();
    }

    @Override
    public String getType(final int position) {
        return CollectionUtilsKt.isEmpty(map) ? "" : CollectionUtilsKt.getValue(map, position);
    }

    @Override
    public String getKey(final int position) {
        return CollectionUtilsKt.isEmpty(map) ? null : CollectionUtilsKt.getKey(map, position);
    }

    @Override
    public int getLayout(final int itemViewType) {
        return itemViewType == RowType.EVEN_ROW.getRowType() ? R.layout.list_item_even : R.layout.list_item_odd;
    }

    @Override
    public int getItemViewType(final int position) {
        return isViewEven(position) ? RowType.EVEN_ROW.getRowType() : RowType.ODD_ROW.getRowType();
    }

    @Override
    public boolean isViewEven(final int position) {
        return position % 2 == 0;
    }
}
