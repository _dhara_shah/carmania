package com.dhara.carmania.type.datasource

import com.dhara.carmania.network.Listener
import com.dhara.carmania.network.entity.TypeResponse

interface TypeDataSource {
    fun fetchTypes(key: String,
                   manufacturer: Long,
                   page: Int,
                   pageSize: Long,
                   listener: Listener<TypeResponse>)
}