package com.dhara.carmania.type.router;

import android.os.Bundle;
import android.view.View;

import com.dhara.carmania.BaseActivity;
import com.dhara.carmania.builds.view.BuildFragment;
import com.dhara.carmania.utils.ConstIntentExtrasKt;

import java.util.List;

import javax.inject.Inject;

public class TypeRouterImpl implements TypeRouter {
    private final BaseActivity activity;

    @Inject
    public TypeRouterImpl(final BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public void gotoPage(final List<View> viewList, final String type, final long manufacturer) {
        final Bundle bundle = new Bundle();
        bundle.putString(ConstIntentExtrasKt.EXTRA_MAIN_TYPE, type);
        bundle.putLong(ConstIntentExtrasKt.EXTRA_MANUFACTURER, manufacturer);
        activity.switchFragment(BuildFragment.class, bundle, viewList);
    }
}
