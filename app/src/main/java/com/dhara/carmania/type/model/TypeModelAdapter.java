package com.dhara.carmania.type.model;

public interface TypeModelAdapter {
    int getCount();

    String getType(int position);

    String getKey(int position);

    int getLayout(int viewType);

    int getItemViewType(int position);

    boolean isViewEven(int position);

}
