package com.dhara.carmania.type.view

import android.view.View
import com.dhara.carmania.common.view.BaseView
import com.dhara.carmania.type.model.TypeModelAdapter

interface TypeView: BaseView {
    fun initViews(modelAdapter: TypeModelAdapter, listener: ViewInteractionListener)

    fun updateViews(modelAdapter: TypeModelAdapter, listener: ViewInteractionListener)

    interface ViewInteractionListener {
        fun onTypeClicked(view: View, key: String)
    }
}