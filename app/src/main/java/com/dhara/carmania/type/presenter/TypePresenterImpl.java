package com.dhara.carmania.type.presenter;

import android.view.View;

import com.dhara.carmania.common.ResponseListener;
import com.dhara.carmania.network.ServiceError;
import com.dhara.carmania.type.model.TypeInteractor;
import com.dhara.carmania.type.model.TypeModelAdapterImpl;
import com.dhara.carmania.type.router.TypeRouter;
import com.dhara.carmania.type.view.TypeView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class TypePresenterImpl implements TypePresenter, ResponseListener, TypeView.ViewInteractionListener {
    private TypeView view;
    private TypeRouter router;
    private TypeInteractor interactor;

    @Override
    public void handleOnCreate(final TypeView view, final TypeInteractor interactor, final TypeRouter router) {
        this.view = view;
        this.interactor = interactor;
        this.router = router;

        view.initViews(new TypeModelAdapterImpl(interactor.getTypes()), this);
        view.showProgress();
        interactor.fetchTypes(this);
    }

    @Override
    public void onSuccess() {
        view.hideProgress();
        view.updateViews(new TypeModelAdapterImpl(interactor.getTypes()), this);
    }

    @Override
    public void onError(@NotNull final ServiceError error) {
        view.hideProgress();
        view.showMessage(error.getErrorMessage());
    }

    @Override
    public void onTypeClicked(final View view, final String key) {
        final List<View> viewList = new ArrayList<>();
        viewList.add(view);

        router.gotoPage(viewList, key, interactor.getManufacturer());
    }
}
