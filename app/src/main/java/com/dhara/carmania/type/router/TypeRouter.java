package com.dhara.carmania.type.router;

import android.view.View;

import java.util.List;

public interface TypeRouter {
    void gotoPage(List<View> viewList, String type, long manufacturer);
}
