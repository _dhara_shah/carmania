package com.dhara.carmania.type.datasource

import com.dhara.carmania.CarManiaApp
import com.dhara.carmania.dagger2.injectors.getNetComponent
import com.dhara.carmania.network.Listener
import com.dhara.carmania.network.ServiceError
import com.dhara.carmania.network.api.RestApi
import com.dhara.carmania.network.entity.TypeResponse
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TypeDataSourceImpl @Inject constructor() : TypeDataSource {
    @Inject
    lateinit var restApi: RestApi

    init {
        getNetComponent(CarManiaApp.instance).inject(this)
    }

    override fun fetchTypes(key: String,
                            manufacturer: Long,
                            page: Int,
                            pageSize: Long,
                            listener: Listener<TypeResponse>) {
        restApi.getTypes(key, manufacturer, page, pageSize)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<TypeResponse> {
                    override fun onSuccess(response: TypeResponse) {
                        listener.onSuccess(response)
                    }

                    override fun onError(e: Throwable) {
                        listener.onError(ServiceError(e.message))
                    }

                    override fun onSubscribe(d: Disposable) {
                    }
                })
    }
}