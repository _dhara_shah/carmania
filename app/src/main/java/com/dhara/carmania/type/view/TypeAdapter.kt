package com.dhara.carmania.type.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dhara.carmania.common.entity.RowType
import com.dhara.carmania.type.model.TypeModelAdapter
import kotlinx.android.synthetic.main.list_item_even.view.*
import kotlinx.android.synthetic.main.list_item_odd.view.*

class TypeAdapter(private val modelAdapter: TypeModelAdapter,
                  private val listener: TypeView.ViewInteractionListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutRes = modelAdapter.getLayout(viewType)
        val itemView = LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
        return if (viewType == RowType.EVEN_ROW.rowType) TypeEvenVH(itemView, modelAdapter, listener)
        else TypeOddVH(itemView, modelAdapter, listener)
    }

    override fun getItemCount(): Int {
        return modelAdapter.count
    }

    override fun getItemViewType(position: Int): Int {
        return modelAdapter.getItemViewType(position)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (modelAdapter.isViewEven(position)) {
            (holder as TypeEvenVH).txtType?.text = modelAdapter.getType(position)
        } else {
            (holder as TypeOddVH).txtType?.text = modelAdapter.getType(position)
        }
    }

    class TypeEvenVH(itemView: View,
                     modelAdapter: TypeModelAdapter,
                     listener: TypeView.ViewInteractionListener): RecyclerView.ViewHolder(itemView) {
        val txtType: TextView? = itemView.txtListItem

        init {
            txtType?.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    listener.onTypeClicked(itemView, modelAdapter.getKey(adapterPosition))
                }
            }
        }
    }

    class TypeOddVH(itemView: View,
                    modelAdapter: TypeModelAdapter,
                    listener: TypeView.ViewInteractionListener): RecyclerView.ViewHolder(itemView) {
        val txtType: TextView? = itemView.txtListItemOdd

        init {
            txtType?.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    listener.onTypeClicked(itemView, modelAdapter.getKey(adapterPosition))
                }
            }
        }
    }
}