package com.dhara.carmania.type.view

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.dhara.carmania.type.model.TypeModelAdapter
import com.dhara.carmania.utils.extensions.setVisible
import com.dhara.carmania.utils.extensions.showSnackBar
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

class TypeViewImpl @Inject constructor(private val activity: AppCompatActivity) : TypeView {
    override fun initViews(modelAdapter: TypeModelAdapter, listener: TypeView.ViewInteractionListener) {
        initToolbar()

        val adapter = TypeAdapter(modelAdapter, listener)
        val layoutManager = LinearLayoutManager(activity.applicationContext).apply {
            orientation = LinearLayoutManager.VERTICAL
        }
        activity.rvItems.layoutManager = layoutManager
        activity.rvItems.setHasFixedSize(true)
        activity.rvItems.adapter = adapter
    }

    override fun updateViews(modelAdapter: TypeModelAdapter, listener: TypeView.ViewInteractionListener) {
        val adapter = TypeAdapter(modelAdapter, listener)
        activity.rvItems.swapAdapter(adapter, true)
    }

    override fun showProgress() {
        activity.progressBar.setVisible(true)
    }

    override fun hideProgress() {
        activity.progressBar.setVisible(false)
    }

    override fun showMessage(message: String?) {
        activity.rvItems.showSnackBar(message)
    }

    private fun initToolbar() {
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        activity.supportActionBar?.setHomeButtonEnabled(true)
    }
}