package com.dhara.carmania;

import android.app.Application;
import android.content.Context;
import android.support.annotation.CallSuper;

import com.dhara.carmania.dagger2.components.CarManiaComponent;
import com.dhara.carmania.dagger2.components.DaggerCarManiaComponent;
import com.dhara.carmania.dagger2.components.DaggerNetComponent;
import com.dhara.carmania.dagger2.components.NetComponent;
import com.dhara.carmania.dagger2.injectors.CarManiaInjectorKt;
import com.dhara.carmania.dagger2.modules.AppModule;
import com.dhara.carmania.dagger2.modules.CarManiaModule;
import com.dhara.carmania.dagger2.modules.NetModule;
import com.dhara.carmania.network.api.ApiKt;
import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CarManiaInjectorKt.class, Retrofit.class, Cache.class, NetModule.class, AppModule.class, CarManiaModule.class})
public class CarManiaBaseTest {
    @Mock
    protected NetModule netModule;
    @Mock
    protected AppModule appModule;
    @Mock
    protected CarManiaModule carManiaModule;
    @Mock
    protected Retrofit retrofit;
    @Mock
    protected OkHttpClient client;
    protected Application application;
    @Mock
    protected Cache cache;
    protected CarManiaComponent carManiaComponent;
    protected NetComponent netComponent;

    @Before
    @CallSuper
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        application = CarManiaApp.Companion.getInstance();
        carManiaComponent = DaggerCarManiaComponent
                .builder()
                .carManiaModule(carManiaModule)
                .appModule(appModule)
                .build();
        PowerMockito.mockStatic(CarManiaInjectorKt.class, Retrofit.class, Cache.class,
                NetModule.class, AppModule.class, CarManiaModule.class);
        PowerMockito.when(CarManiaInjectorKt.getCarManiaComponent(any(Context.class))).thenReturn(carManiaComponent);

        netComponent = DaggerNetComponent.builder()
                .appModule(appModule)
                .netModule(netModule)
                .build();

        netModule.setBaseUrl(ApiKt.getHost());

        when(netModule.provideRetrofit(Matchers.any(Gson.class), Matchers.any(OkHttpClient.class))).thenReturn(retrofit);
        when(netModule.provideGson()).thenReturn(new Gson());
        when(netModule.provideHttpCache(application)).thenReturn(cache);
        when(netModule.provideOkhttpClient(cache)).thenReturn(client);
    }

    @Test
    public void emptyTest() {
        //to compile with Runner
    }
}
