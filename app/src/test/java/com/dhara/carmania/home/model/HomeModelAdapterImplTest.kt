package com.dhara.carmania.home.model

import com.dhara.carmania.R
import com.dhara.carmania.common.entity.RowType
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

private const val POSITION = 0
private const val MANUFACTURER = "BMW"
private const val MANUFACTURER_ID = 101
private const val VIEW_TYPE = 0
private const val ROW_POSITION_EVEN = 2
private const val ROW_POSITION_ODD = 1

class HomeModelAdapterImplTest {
    private lateinit var modelAdapter: HomeModelAdapterImpl

    @Mock
    lateinit var map: MutableMap<Int, String>
    @Mock
    lateinit var set: MutableSet<MutableMap.MutableEntry<Int, String>>
    @Mock
    lateinit var entry: MutableMap.MutableEntry<Int, String>
    @Mock
    lateinit var iterator: MutableIterator<MutableMap.MutableEntry<Int, String>>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        modelAdapter = HomeModelAdapterImpl(map)
    }

    @Test
    fun testGetCount() {
        Mockito.`when`(map.isEmpty()).thenReturn(false)
        Mockito.`when`(map.entries).thenReturn(set)
        Mockito.`when`(set.size).thenReturn(1)
        modelAdapter.count
        verify(map).isEmpty()
        verify(map).entries
        assertEquals(map.entries.size, 1)
    }

    @Test
    fun testGetManufacturer() {
        Mockito.`when`(map.isEmpty()).thenReturn(false)
        Mockito.`when`(map.entries).thenReturn(set)
        Mockito.`when`(set.size).thenReturn(1)
        Mockito.`when`(entry.value).thenReturn(MANUFACTURER)
        Mockito.`when`(set.iterator()).thenReturn(iterator)
        Mockito.`when`(iterator.hasNext()).thenReturn(true)
        Mockito.`when`(iterator.next()).thenReturn(entry)

        set.add(entry)

        modelAdapter.getManufacturer(POSITION)
        verify(map).isEmpty()
        verify(map).entries
        verify(set).iterator()
        verify(iterator).hasNext()
        verify(iterator).next()
        verify(entry).value
        assertEquals(map.entries.elementAt(POSITION).value, MANUFACTURER)
    }

    @Test
    fun testGetKey() {
        Mockito.`when`(map.isEmpty()).thenReturn(false)
        Mockito.`when`(map.entries).thenReturn(set)
        Mockito.`when`(set.size).thenReturn(1)
        Mockito.`when`(entry.key).thenReturn(MANUFACTURER_ID)
        Mockito.`when`(set.iterator()).thenReturn(iterator)
        Mockito.`when`(iterator.hasNext()).thenReturn(true)
        Mockito.`when`(iterator.next()).thenReturn(entry)

        set.add(entry)

        modelAdapter.getKey(POSITION)
        verify(map).isEmpty()
        verify(map).entries
        verify(set).iterator()
        verify(iterator).hasNext()
        verify(iterator).next()
        verify(entry).key
        assertEquals(map.entries.elementAt(POSITION).key, MANUFACTURER_ID)
    }

    @Test
    fun testGetLayout() {
        assertEquals(modelAdapter.getLayout(VIEW_TYPE), R.layout.list_item_even)
    }

    @Test
    fun testGetItemViewTypeEven() {
        assertEquals(modelAdapter.getItemViewType(ROW_POSITION_EVEN), RowType.EVEN_ROW.rowType)
        assertEquals(ROW_POSITION_EVEN % 2, 0)
    }

    @Test
    fun testGetItemViewTypeOdd() {
        assertEquals(modelAdapter.getItemViewType(ROW_POSITION_ODD), RowType.ODD_ROW.rowType)
        assertNotEquals(ROW_POSITION_ODD % 2, 0)
    }

    @Test
    fun testIsViewEven() {
        assertEquals(modelAdapter.isViewEven(ROW_POSITION_EVEN), true)
    }

    @Test
    fun testIsViewOdd() {
        assertEquals(modelAdapter.isViewEven(ROW_POSITION_ODD), false)
    }
}