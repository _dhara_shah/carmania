package com.dhara.carmania.home.presenter

import com.dhara.carmania.CarManiaBaseTest
import com.dhara.carmania.home.model.HomeInteractor
import com.dhara.carmania.home.model.HomeModelAdapter
import com.dhara.carmania.home.router.HomeRouter
import com.dhara.carmania.home.view.HomeView
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.eq
import org.mockito.Captor
import com.dhara.carmania.common.ResponseListener
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import org.junit.After

class HomePresenterImplTest: CarManiaBaseTest() {
    private lateinit var homePresenter: HomePresenterImpl
    @Mock
    lateinit var homeView: HomeView

    @Mock
    lateinit var interactor: HomeInteractor

    @Mock
    lateinit var router: HomeRouter

    @Captor
    lateinit var modelAdapter: ArgumentCaptor<HomeModelAdapter>

    @Captor
    lateinit var responseListener: ArgumentCaptor<ResponseListener>


    @Before
    @Throws(Exception::class)
    override fun setUp() {
        super.setUp()
        MockitoAnnotations.initMocks(this)
        Mockito.`when`(carManiaModule.provideHomeInteractor()).thenReturn(interactor)
        homePresenter = HomePresenterImpl()
    }

    @Test
    fun testHandleOnCreate() {
        homePresenter.handleOnCreate(homeView, router)
        verify(homeView).initViews(modelAdapter.capture(), eq(homePresenter))
        verify(homeView).showProgress()
        verify(interactor).fetchManufacturers(responseListener.capture(), eq(true))
    }

    @After
    fun testAfter() {
        verifyNoMoreInteractions(homeView, router, interactor)
    }
}