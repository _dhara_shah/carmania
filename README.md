# CarMania

1. This application allows users to pick the manufacturers, the main types and the build dates 
2. Supports both the landscape and portrait mode
3. Follows the MVP pattern with Interactors and Routers, in Java and kotlin
4. The list transitions are animated

# Libraries used
1. Retrofit
2. Gson converter , Ok HTTP
3. Support libraries, Design, constraintLayout, RecyclerView
4. Unit testing 
5. Mockito and Powermockito
7. Dagger2

# Things not completed
1. Selection mode (item selected is not displayed as selected)
2. When the user scrolls down, the data does load more, but then it does not position correctly (bug)
3. On the third screen, we need to display the summary after selecting the build date - it is pending
4. Only HomeModelAdapterImplTest class runs without issues, 
the other class written has some issues i need to look at.
5. Only the home screen has a load more
6. If the screen orientation is changed, load more does not function

# APKs

The debug apk is attached to this source code
